import { Form } from "@/components";

const AuthPage = () => {
  return (
    <main className="w-full">
      <Form />
    </main>
  );
};

export default AuthPage;
