import { BrowserRouter, Routes, Route } from "react-router-dom";

import { Suspense, lazy } from "react";

const AuthPage = lazy(() => import("./pages/AuthPage/AuthPage"));

function App() {
  return (
    <Suspense fallback={<div>Loading...</div>}>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<AuthPage />} />
        </Routes>
      </BrowserRouter>
    </Suspense>
  );
}

export default App;
