import { FC, useState } from "react";
import { useForm, SubmitHandler } from "react-hook-form";

import { Input } from "@chakra-ui/react";
//@ts-ignore
import { MultiSelect, useMultiSelect } from "chakra-multiselect";

import { IUser } from "@/types";

type Props = {
  isRegister?: boolean;
};

export const Form: FC<Props> = (props) => {
  const [isRegister, setIsRegister] = useState<boolean>(
    props.isRegister ? true : false
  );
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<IUser>();

  const onSubmit: SubmitHandler<IUser> = (data) => {
    const values: IUser = {
      ...data,
      interests: value,
    };
    console.log(values);
  };
  const { value, options, onChange } = useMultiSelect({
    value: [],
    options: ["Напишите интересы"],
  });

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      {isRegister ? (
        <>
          <Input {...register("username", { required: true })} />
          {errors.username && (
            <p className="text-red-600">Username is required</p>
          )}
          <Input
            {...register("password", {
              required: true,
              pattern: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/,
            })}
            type="password"
          />
          {errors.password && (
            <p className="text-red-600">
              The password you provided does not meet the required criteria.
            </p>
          )}
          <MultiSelect
            label="Интересы"
            value={value}
            onChange={onChange}
            options={options}
            create
            multi
          />
          <button type="submit">Register</button>
        </>
      ) : (
        <>
          <Input {...register("username")} />
          <Input {...register("password")} type="password" />
          <button type="submit">Login</button>
          <span
            onClick={() => {
              setIsRegister(true);
            }}
            className="cursor-pointer w-full text-blue-400 underline"
          >
            Do not have account
          </span>
        </>
      )}
    </form>
  );
};
