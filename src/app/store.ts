import { configureStore, combineReducers } from "@reduxjs/toolkit";

import { baseApi } from "./services/baseApi";

const rootReducers = combineReducers({
  [baseApi.reducerPath]: baseApi.reducer,
});

export const store = configureStore({
  reducer: rootReducers,
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(),
});
