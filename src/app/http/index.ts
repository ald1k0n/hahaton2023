import axios from "axios";

export const API_URL = "http://25.27.0.102/api";

export const axiosInstance = axios.create({
  baseURL: API_URL,
  withCredentials: true,
});
